function finalTexture = EfrosLeung(Texture, tailleResultat)
% Efros-Leung algorithm
%   Implementation of Efros-Leung algorithm to extend a little texture to
%   an infinite texture surface

lignes = size(Texture, 1);
colonnes = size(Texture, 2);

% Setting final Image matrix
finalTexture = zeros(tailleResultat, tailleResultat, 3);

% Setting filled pixels matrix
pixelRempli = zeros(tailleResultat);
% Setting filled neighboring pixels matrix
pixelsVoisinsRempli = zeros(tailleResultat);

% Setting convolution matrix to count filled neighbor pixels
matriceConvolution = ones(3);
matriceConvolution(2, 2) = 0;

% Find a random position in image to take a patch
randRow = floor(rand() * (lignes - 3));
randColumn = floor(rand() * (colonnes - 3));

% Extracting the patch
patch = Texture(randRow : randRow + 2, randColumn : randColumn + 2, :);

% Placing the patch at result Image center
finalTexture( floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1, floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1, :) = patch;

% Setting pixels filled
pixelRempli( floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1, floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1) = 1;

% Setting filled neighboring pixels by executing a convolution
pixelsVoisinsRempli = conv2(pixelRempli, matriceConvolution, 'same');

% Setting neighboring count of filled pixels to 0
% -> éviter de prendre un pixel rempli quand on sélectionne le pixel ayant
% le maximum de voisins remplis
pixelsVoisinsRempli(floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1, floor(tailleResultat / 2) - 1 : floor(tailleResultat / 2) + 1) = 0;

% Need to store filled pixels count to know when to stop
nbPixelsRemplis = 9;

% Maximum pixels count
nbPixelFinalTexture = tailleResultat * tailleResultat - tailleResultat*2 - (tailleResultat-2)*2;

while nbPixelsRemplis < nbPixelFinalTexture
    
    %on exclut les pixels en bordure de l'image
    matriceSansBords = pixelsVoisinsRempli(2 : tailleResultat - 1, 2 : tailleResultat - 1);
    maximum = max(max(matriceSansBords));
    [X, Y] = find(matriceSansBords == maximum);
    
    x = X(1)+1;
    y = Y(1)+1;
    
    patch = finalTexture(x - 1 : x + 1, y - 1 : y + 1, :);
    patchPixelRempli = pixelRempli(x - 1 : x + 1, y - 1 : y + 1);
    
    Distances = Inf(lignes, colonnes);
    for i =  2 : lignes - 1
       for j = 2 : colonnes - 1
           patchCourantTexture = Texture(i - 1 : i + 1, j - 1 : j + 1, :);
           
           patchResultat = ( (patch - patchCourantTexture) .^2 ) .* patchPixelRempli;
          
           Distances(i, j) = sum(patchResultat(:));
       end
    end
    minimum = min(min(Distances));
    [Xdistance, Ydistance] = find(Distances == minimum);
   
    x_bestdistance = Xdistance(1);
    y_bestdistance = Ydistance(1);
    
    finalTexture(x, y) = Texture(x_bestdistance, y_bestdistance);
    
    pixelRempli(x, y) = 1;
    % Setting filled neighboring pixels by executing a convolution
    pixelsVoisinsRempli = conv2(pixelRempli, matriceConvolution, 'same') .* not(pixelRempli);
    
    nbPixelsRemplis = nbPixelsRemplis + 1;
    
    image(finalTexture); drawnow;
   
end

end

