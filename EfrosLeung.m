function finalTexture = EfrosLeung(Texture, tailleResultat, tailleVoisinage, epsilon)
% Efros-Leung algorithm
%   Implementation of Efros-Leung algorithm to extend a little texture to
%   an infinite texture surface

lignes = size(Texture, 1);
colonnes = size(Texture, 2);

% Setting final Image matrix
finalTexture = zeros(tailleResultat, tailleResultat, 3);

% Setting filled pixels matrix
pixelRempli = zeros(tailleResultat);
% Setting filled neighboring pixels matrix
pixelsVoisinsRempli = zeros(tailleResultat);

% Setting convolution matrix to count filled neighbor pixels
matriceConvolution = ones(tailleVoisinage);
matriceConvolution(1 + floor(tailleVoisinage/2), 1 + floor(tailleVoisinage/2)) = 0;

% Find a random position in image to take a patch
randColumn = floor(rand() * (colonnes - (tailleVoisinage + 1)));
randRow = floor(rand() * (lignes - (tailleVoisinage + 1)));

% Extracting the patch
patch = Texture(randRow : randRow + (tailleVoisinage-1), randColumn : randColumn + (tailleVoisinage-1), :);

% Placing the patch at result Image center
finalTexture(tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2), tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2), :) = patch;

% Setting pixels filled
pixelRempli(tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2), tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2)) = 1;

% Setting filled neighboring pixels by executing a convolution
pixelsVoisinsRempli = conv2(pixelRempli, matriceConvolution, 'same');

% Setting neighboring count of filled pixels to 0
% -> éviter de prendre un pixel rempli quand on sélectionne le pixel ayant
% le maximum de voisins remplis
pixelsVoisinsRempli(tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2), tailleResultat / 2 - floor(tailleVoisinage/2) : tailleResultat / 2 + floor(tailleVoisinage/2)) = 0;

% Need to store filled pixels count to know when to stop
nbPixelsRemplis = tailleVoisinage * tailleVoisinage;

% Maximum pixels count
% On enlève les pixels au bord
nbPixelFinalTexture = tailleResultat * tailleResultat - tailleResultat*2 - (tailleResultat-2)*2;

while nbPixelsRemplis < nbPixelFinalTexture
    
    %on exclut les pixels en bordure de l'image
    matriceSansBords = pixelsVoisinsRempli((tailleVoisinage + 1) / 2 : tailleResultat - floor(tailleVoisinage/ 2), (tailleVoisinage + 1) / 2 : tailleResultat - floor(tailleVoisinage/ 2));
    maximum = max(max(matriceSansBords));
    [X, Y] = find(matriceSansBords == maximum);
   
    x = X(1)+floor(tailleVoisinage/2);
    y = Y(1)+floor(tailleVoisinage/2);
    
    patch = finalTexture(x - floor(tailleVoisinage / 2) : x + floor(tailleVoisinage / 2), y - floor(tailleVoisinage / 2) : y + floor(tailleVoisinage / 2), :);
    patchPixelRempli = pixelRempli(x - floor(tailleVoisinage / 2) : x + floor(tailleVoisinage / 2), y - floor(tailleVoisinage / 2) : y + floor(tailleVoisinage / 2));
    
    Distances = Inf(lignes, colonnes);
    for i =  (tailleVoisinage + 1) / 2 : lignes - floor(tailleVoisinage/ 2)
       for j = (tailleVoisinage + 1) / 2 : colonnes - floor(tailleVoisinage/ 2)
           patchCourantTexture = Texture(i - floor(tailleVoisinage / 2) : i + floor(tailleVoisinage / 2), j - floor(tailleVoisinage / 2) : j + floor(tailleVoisinage / 2), :);
           
           patchResultat = ( (patch - patchCourantTexture).*(patch - patchCourantTexture) ) .* patchPixelRempli;
           
           Distances(i, j) = sum(sum(sum(patchResultat)));
       end
    end
    
    minimum = min(min(Distances));    
    
    [Xbest, Ybest] = find(Distances == minimum);
    
    % version ou on choisit le pixel ayant le patch de distance minimum
   
    x_bestdistance = Xbest(1);
    y_bestdistance = Ybest(1);
    
    finalTexture(x, y) = Texture(x_bestdistance, y_bestdistance);
    
    % version ou on choisit le pixel d'un patch choisit aleatoirement parmi
    % les patch de distance <= (1 + epsilon) * distance minimum
    
%     [XdistanceEpsilon, YdistanceEpsilon] = find(Distances <= (1+epsilon)*minimum);
%     
%     randomIndex = floor(rand() * size(XdistanceEpsilon, 1)) + 1;
%     x_random = XdistanceEpsilon(randomIndex);
%     randomIndex = floor(rand() * size(YdistanceEpsilon, 1)) + 1;
%     y_random = YdistanceEpsilon(randomIndex);
%     
%     finalTexture(x, y) = Texture(x_random, y_random);
    
    pixelRempli(x, y) = 1;
    % Setting filled neighboring pixels by executing a convolution
    pixelsVoisinsRempli = conv2(pixelRempli, matriceConvolution, 'same') .* not(pixelRempli);
    
    nbPixelsRemplis = nbPixelsRemplis + 1;
    
    image(finalTexture); drawnow;
   
end

end

